import ui.ConsoleUI;

public class App {
    public static void main(String[] args) throws Exception {
        ConsoleUI ui = new ConsoleUI();
        ui.start();
    }
}
